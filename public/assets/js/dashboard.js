$(function () {
    $(".alert")
        .delay(4000)
        .slideUp(200, function () {
            $("#alert alert-success")
                .fadeTo(2000, 500)
                .slideUp(500, function () {
                    $("#alert alert-success").slideUp(500);
                });

            $("#alert alert-danger")
                .fadeTo(2000, 500)
                .slideUp(500, function () {
                    $("#alert alert-danger").slideUp(500);
                });
        });
});
