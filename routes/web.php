<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TestCaseTNRController;
use App\Http\Controllers\TestCaseController;
use App\Http\Controllers\ExportAsCSVController;
use App\Http\Controllers\ImportCustomPropertiesFile;
use App\Http\Controllers\FareFamilyController;
use App\Http\Controllers\AncillariesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::post('export-as-csv', [ExportAsCSVController::class, 'export'])->name('export.as.csv');
Route::post('change-language', [HomeController::class, 'changeLang'])->name('change.lang');
Route::resource('import-custom-properties-file', ImportCustomPropertiesFile::class);
Route::resource('test-number', TestCaseController::class)->parameters(['test-number' => 'TestNumber']);
Route::post('delete-all-test-number', [TestCaseController::class, 'destroyAll'])->name('destroy.all.tn');
Route::resource('ancillaries-name', AncillariesController::class);
Route::resource('fare-family', FareFamilyController::class);
Route::resource('tnr', TestCaseTNRController::class)->parameters(["tnr" => "cid"]);
Route::post('delete-all-tnr', [TestCaseTNRController::class, 'destroyAll'])->name('destroy.all.tnr');
