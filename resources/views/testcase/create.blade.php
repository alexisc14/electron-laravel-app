@extends('layouts.master')
@section('title', '| ' . __('PageTitle')['testTitle'])
@section('header-scripts')

@endsection

@section('content')
    @if ($message = Session::get('message'))
        <div class="alert alert-success" id="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger" id="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif

    <div class="row">
        <div class="col-lg-10">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title fw-semibold mb-4">{{ __('PageTitle')['testTitle'] }}</h5>
                    <form method="POST" class="forms-sample" id="testCaseAddForm"
                        action="{{ route('test-number.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">TestNumber *</label>
                            <input type="number" class="form-control" id="TestNumber" aria-describedby="emailHelp"
                                name="TestNumber">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">CorrelationID *</label>
                            <input type="text" class="form-control" id="correlation_id" name="correlation_id">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">IATA *</label>
                            <input type="text" class="form-control" id="iata_agency" name="iata_agency">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Airline *</label>
                            <input type="text" class="form-control" id="airline" name="airline">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Pseudo city code *</label>
                            <input type="text" class="form-control" id="pseudo_city" name="pseudo_city">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Travel agency name *</label>
                            <input type="text" class="form-control" id="travel_agency_name" name="travel_agency_name">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Cabin type name *</label>
                            <select id="cabin_type_Name" name="cabin_type_Name" class="form-select">
                                <option disabled selected>
                                    {{ __('TestNumberPage')['testNumbersFormCabinTypeNamePlaceholder'] }}</option>
                                <option>ECONOMY</option>
                                <option>PREMIUM</option>
                                <option>BUSINESS</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Strict search cabin</label>
                            <select id="StrictSearchCabin" name="StrictSearchCabin" class="form-select">
                                <option disabled selected>
                                    {{ __('TestNumberPage')['testNumbersFormStrictCabinTypeNamePlaceholder'] }}</option>
                                <option>ECONOMY</option>
                                <option>PREMIUM</option>
                                <option>BUSINESS</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Strict search FF</label>
                            <select id="StrictSearchFF" name="StrictSearchFF" class="form-select">
                                <option disabled selected>
                                    {{ __('TestNumberPage')['testNumbersFormStrictFFNamePlaceholder'] }}</option>
                                @foreach ($fareFamilies as $data)
                                    <option>
                                        {{ $data->cabin_type . ' - ' . $data->fare_family_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="pax" class="form-label">Pax *</label>
                            <input type="text" class="form-control" id="pax" name="pax">
                            <div id="paxHelp" class="form-text">{{ __('TestNumberPage')['testNumbersFormPaxCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="OnD" class="form-label">OnD *</label>
                            <input type="text" class="form-control" id="OnD" name="OnD">
                            <div id="OnDHelp" class="form-text">{{ __('TestNumberPage')['testNumbersFormOnDCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="OnDRebooking" class="form-label">OnDRebooking</label>
                            <input type="text" class="form-control" id="OnDRebooking" name="OnDRebooking">
                            <div id="OnDRebookingHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormOnDCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="Date" class="form-label">Date *</label>
                            <input type="text" class="form-control" id="Date" name="Date">
                            <div id="DateHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormDateCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="DateRebooking" class="form-label">Date rebooking</label>
                            <input type="text" class="form-control" id="DateRebooking" name="DateRebooking">
                            <div id="DateRebookingHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormDateCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="bundle_seats" class="form-label">Bundle seats</label>
                            <input type="text" class="form-control" id="bundle_seats" name="bundle_seats">
                            <div id="bundle_seatsHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="upsell" class="form-label">Upsell</label>
                            <input type="text" class="form-control" id="upsell" name="upsell">
                            <div id="upsellHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="bundleSeatAtUpsell" class="form-label">Bundle seat upsell</label>
                            <input type="text" class="form-control" id="bundleSeatAtUpsell"
                                name="bundleSeatAtUpsell">
                            <div id="bundleSeatAtUpsellHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="GST_Tax" class="form-label">GST tax</label>
                            <input type="text" class="form-control" id="GST_Tax" name="GST_Tax">
                            <div id="GST_TaxHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="countrycode" class="form-label">Country code</label>
                            <input type="text" class="form-control" id="countrycode" name="countrycode">
                            <div id="countrycodeHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormCountryCodeCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="paymentMethod" class="form-label">Payment method</label>
                            <select id="paymentMethod" name="paymentMethod" class="form-select">
                                <option disabled selected selected>
                                    {{ __('TestNumberPage')['testNumbersFormPaymentMethodCaption'] }}</option>
                                <option>VI</option>
                                <option>AX</option>
                                <option>EP</option>
                                <option>DC</option>
                                <option>CA</option>
                                <option>JCB</option>
                                <option>TP</option>
                                <option>VOUCHER</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="recognition" class="form-label">Recognition</label>
                            <input type="text" class="form-control" id="recognition" name="recognition">
                            <div id="recognitionHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="MultiplePricingProgram" class="form-label">Multiple pricing program</label>
                            <input type="text" class="form-control" id="MultiplePricingProgram"
                                name="MultiplePricingProgram">
                            <div id="MultiplePricingProgramHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormMultiplePricingProgramCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="pricing_program" class="form-label">Pricing program</label>
                            <select id="pricing_program" name="pricing_program" class="form-select">
                                <option disabled selected>
                                    {{ __('TestNumberPage')['testNumbersFormPricingProgramCaption'] }}</option>
                                <option>CORPORATE</option>
                                <option>SC</option>
                            </select>
                            <div id="pricing_programHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormPricingProgramCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="account_code" class="form-label">Account code</label>
                            <input type="text" class="form-control" id="account_code" name="account_code">
                            <div id="account_codeHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormBundleSeatOrUpsellOrGstTaxOrRecognitionOrAccountCodeCaption'] }}
                            </div>

                        </div>
                        <div class="mb-3">
                            <label for="OIN" class="form-label">OIN</label>
                            <input type="text" class="form-control" id="OIN" name="OIN">
                            <div id="OINHelp" class="form-text">{{ __('TestNumberPage')['testNumbersFormOINCaption'] }}
                            </div>

                        </div>
                        <div class="mb-3">
                            <label for="order_id" class="form-label">OrderID</label>
                            <input type="text" class="form-control" id="order_id" name="order_id">
                            <div id="order_idHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormOrderIDCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="private_fare" class="form-label">Private fare</label>
                            <input type="text" class="form-control" id="private_fare" name="private_fare">
                            <div id="private_fareHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormPrivateFareCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="Bag" class="form-label">Bag</label>
                            <select id="Bag" name="Bag" class="form-select">
                                <option disabled selected selected>{{ __('TestNumberPage')['testNumbersFormBagCaption'] }}
                                </option>
                                @foreach ($ancillariesName as $data)
                                    @if ($data->type === 'BAGS')
                                        <option value="{{ $data->name }}">
                                            @if($data->name_description != ""){{ $data->name . ' - ' . $data->name_description }}@else{{ $data->name }}@endif
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Bag rebook</label>
                            <select id="BagRebook" name="BagRebook" class="form-select">
                                <option disabled selected>{{ __('TestNumberPage')['testNumbersFormBagRebookingCaption'] }}
                                </option>
                                @foreach ($ancillariesName as $data)
                                    @if ($data->type === 'BAGS')
                                        <option value="{{ $data->name }}">
                                            @if($data->name_description != ""){{ $data->name . ' - ' . $data->name_description }}@else{{ $data->name }}@endif
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="Seat" class="form-label">Seat</label>
                            <select id="Seat" name="Seat" class="form-select">
                                <option disabled selected>{{ __('TestNumberPage')['testNumbersFormSeatCaption'] }}</option>
                                @foreach ($ancillariesName as $data)
                                    @if ($data->type === 'SEAT')
                                        <option value="{{ $data->name }}">
                                            @if($data->name_description != ""){{ $data->name . ' - ' . $data->name_description }}@else{{ $data->name }}@endif
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="SeatRebook" class="form-label">Seat rebook</label>
                            <select id="SeatRebook" name="SeatRebook" class="form-select">
                                <option disabled selected>{{ __('TestNumberPage')['testNumbersFormSeatRebookingCaption'] }}
                                </option>
                                @foreach ($ancillariesName as $data)
                                    @if ($data->type === 'SEAT')
                                        <option value="{{ $data->name }}">
                                            @if($data->name_description != ""){{ $data->name . ' - ' . $data->name_description }}@else{{ $data->name }}@endif
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="FareFamilyRebook" class="form-label">Fare family rebook</label>
                            <select id="FareFamilyRebook" name="FareFamilyRebook" class="form-select">
                                <option disabled selected>
                                    {{ __('TestNumberPage')['testNumbersFormFareFamilyRebookCaption'] }}</option>
                                @foreach ($fareFamilies as $data)
                                    <option>
                                        {{ $data->cabin_type . ' - ' . $data->fare_family_name }}</option>
                                @endforeach
                            </select>
                            <div id="FareFamilyRebookHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormFareFamilyRebookCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="APIS" class="form-label">APIS</label>
                            <input type="checkbox" class="form-check-input" id="APIS" name="APIS">
                            <div id="APISHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormAPISOrAfterCreateCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="APIS_after_create" class="form-label">APIS after create</label>
                            <input type="checkbox" class="form-check-input" id="APIS_after_create" name="APIS_after_create">
                            <div id="APIS_after_createHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormAPISOrAfterCreateCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="APIS_type" class="form-label">APIS type</label>
                            <input type="text" class="form-control" id="APIS_type" name="APIS_type">
                            <div id="APIS_typeHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormAPISTypeCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="APIS_update_pax" class="form-label">APIS update pax</label>
                            <input type="text" class="form-control" id="APIS_update_pax" name="APIS_update_pax">
                            <div id="APIS_update_paxHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormAPISUpdatePaxCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="APIS_pax_titlename" class="form-label">APIS pax titlename</label>
                            <input type="text" class="form-control" id="APIS_pax_titlename"
                                name="APIS_pax_titlename">
                            <div id="APIS_pax_titlenameHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormAPISPaxTitlenameCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LoyaltyProgram" class="form-label">Loyalty program</label>
                            <input type="text" class="form-control" id="LoyaltyProgram" name="LoyaltyProgram">
                            <div id="LoyaltyProgramHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LoyaltyProgramAfterBooking" class="form-label">Loyalty program after
                                booking</label>
                            <input type="checkbox" class="form-check-input" id="LoyaltyProgramAfterBooking"
                                name="LoyaltyProgramAfterBooking">
                            <div id="LoyaltyProgramAfterBookingHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramAfterCreateCaption'] }}
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="LoyaltyProgram" class="form-label">Loyalty program step</label>
                            <input type="text" class="form-control" id="LoyaltyProgramStep"
                                name="LoyaltyProgramStep">
                            <div id="LoyaltyProgramStepHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramStepCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LoyaltyProgramTierName" class="form-label">Loyalty program tier
                                name</label>
                            <input type="text" class="form-control" id="LoyaltyProgramTierName"
                                name="LoyaltyProgramTierName">
                            <div id="LoyaltyProgramTierNameHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramTierNameCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LP_account" class="form-label">Loyalty program account</label>
                            <input type="text" class="form-control" id="LP_account" name="LP_account">
                            <div id="LP_accountHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramAccountCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LP_civility" class="form-label">Loyalty program civility</label>
                            <input type="text" class="form-control" id="LP_civility" name="LP_civility">
                            <div id="LP_civilityHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramPaxCivilityCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LP_givenname" class="form-label">Loyalty program
                                givenname</label>
                            <input type="text" class="form-control" id="LP_givenname" name="LP_givenname">
                            <div id="LP_givennameHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramPaxGivennameCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LP_surname" class="form-label">Loyalty program surname</label>
                            <input type="text" class="form-control" id="LP_surname" name="LP_surname">
                            <div id="LP_surnameHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramPaxSurnameCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="LoyaltyProgramUpdatePax" class="form-label">Loyalty program update
                                pax</label>
                            <input type="text" class="form-control" id="LoyaltyProgramUpdatePax"
                                name="LoyaltyProgramUpdatePax">
                            <div id="LoyaltyProgramUpdatePaxHelp" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormLoyaltyProgramUpdatePaxCaption'] }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="SplitUpdatePax" class="form-label">Split - pax to update</label>
                            <input type="text" class="form-control" id="SplitUpdatePax"
                                name="SplitUpdatePax">
                            <div id="SplitUpdatePax" class="form-text">
                                {{ __('TestNumberPage')['testNumbersFormSplitPaxToUpdateCaption'] }}</div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div id="sticky-div" style="position: fixed;">
                <div class="card">
                    <div class="card-body">
                        <button type="submit"
                            class="btn btn-primary">{{ __('FormButton')['formCreateButtonText'] }}</button>
                        </form>
                        <hr>
                        <a href="{{ route('index') }}"
                            class="btn btn-danger">{{ __('FormButton')['formCancelButtonText'] }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script>
        // Cache selectors outside callback for performance. 
        var $window = $(window),
            $stickyEl = $('#sticky-div'),
            elTop = $stickyEl.offset().top;

        $window.scroll(function() {
            $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
        });


        $(".alert").delay(4000).slideUp(200, function() {
            $("#alert alert-success").fadeTo(2000, 500).slideUp(500, function() {
                $("#alert alert-success").slideUp(500);
            });

            $("#alert alert-danger").fadeTo(2000, 500).slideUp(500, function() {
                $("#alert alert-danger").slideUp(500);
            });
        });

        document.addEventListener('DOMContentLoaded', (event) => {
            const inputElement = document.getElementById('TestNumber');
            inputElement.addEventListener('keydown', function(evt) {
                if (["e", "E", "+", "-"].includes(evt.key)) {
                    evt.preventDefault();
                }
            });
        });

        $(document).ready(function() {
            $('#iata_agency').on('change', function() {
                if ($('#iata_agency').val() == "12345675") {
                    $('#airline').val('AF');
                    $('#pseudo_city').val('PAR');
                    $('#travel_agency_name').val('AGENCE TEST');
                } else if ($('#iata_agency').val() == "87654324") {
                    $('#airline').val('KL');
                    $('#travel_agency_name').val('TEST AGENCY');
                    $('#pseudo_city').val('AMS');
                } else {
                    $('#airline').val('');
                    $('#travel_agency_name').val('');
                    $('#pseudo_city').val('');
                }
            });
        });
    </script>
@endsection
