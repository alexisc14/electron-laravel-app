@extends('layouts.master')
@section('title', "| " . __('PageTitle')['fileImportTitle'])
@section('header-scripts')
@endsection

@section('content')

    @if ($message = Session::get('message'))
        <div class="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-error" role="alert">
            {{ $message }}
        </div>
    @endif

    <div class="row">
        <div class="col-lg-10">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title fw-semibold mb-4">{{ __('ImportFilePage')['importFileTitle']}}</h5>
                    <form method="POST" enctype="multipart/form-data" id="formCustomProperties" class="mt-3"
                        action="{{ route('import-custom-properties-file.store') }}">
                        @csrf
                        <div class="mt-3">
                            <input class="form-control" type="file" id="formFile" accept=".csv" name="csvFile"
                                required>
                        </div>
                        <div class="mt-3 form-check">
                            <input type="checkbox" class="form-check-input" id="TNROption">
                            <label class="form-check-label" for="TNROption"><i>{{ __('ImportFilePage')['importFileFormCheckboxLabel']}}</i></label>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div id="sticky-div" style="position: fixed;">
                <div class="card">
                    <div class="card-body">
                        <button type="submit" id="btnImport" class="btn btn-primary"
                            onclick="return confirm('{{ __('ImportFilePage')['importFileFormWarningMessage']}}')">{{ __('ImportFilePage')['importFileFormImportButtonText']}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script>
        $(document).ready(function() {
            $('#btnImport').attr('disabled', true)
            let inputFormFile = $('#formCustomProperties')
            $('input[type=file]').change(function() {
                let value = $('#formFile').val()
                if (!value.endsWith(".csv")) {
                    if ($('#errorMessage')) {
                        $('#errorMessage').remove()
                    }
                    inputFormFile.append("<div id='errorMessage' class='mt-3'>\
                                <span style='color:#FA896B'>{{ __('ImportFilePage')['importFileCSVDetectorMessage']}}</span>\
                            </div>")
                    $('#btnImport').attr('disabled', true)
                } else {
                    if ($('#errorMessage')) {
                        $('#errorMessage').remove()
                        $('#btnImport').attr('disabled', false)
                    }
                }
            })

            $("#TNROption").on('change', function() {
                if (this.checked) {
                    if ($('#modelID')) {
                        $('#modelID').remove()
                    }
                    var $input = $('<input>').attr({
                        id: 'modelID',
                        type: 'hidden',
                        name: 'modelID',
                        value: '2'
                    }).appendTo('#formCustomProperties');
                } else {
                    if ($('#modelID')) {
                        $('#modelID').remove()
                    }
                    var $input = $('<input>').attr({
                        id: 'modelID',
                        type: 'hidden',
                        name: 'modelID',
                        value: '1'
                    }).appendTo('#formCustomProperties');
                }
            });

            $("#btnImport").click(function() {
                if (!$("#TNROption").is(":checked")) {
                    var $input = $('<input>').attr({
                        id: 'modelID',
                        type: 'hidden',
                        name: 'modelID',
                        value: '1'
                    }).appendTo('#formCustomProperties');
                }
            });

        })
    </script>
@endsection
