@extends('layouts.master')
@section('title', "| " . __('PageTitle')['ancillariesTitle'])
@section('header-scripts')
@endsection
@section('content')
    @if ($message = Session::get('message'))
        <div id="toast-notification"
            class="bs-toast toast toast-placement-ex m-2 fade bg-success top-0 start-50 translate-middle-x show"
            role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
            <div class="toast-header">
                <i class="bx bx-bell me-2"></i>
                <div class="me-auto fw-semibold">Message</div>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body"> {{ $message }}
            </div>
        </div>
    @elseif($message = Session::get('error'))
        <div class="bs-toast toast toast-placement-ex m-2 fade bg-danger top-0 start-50 translate-middle-x show"
            role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
            <div class="toast-header">
                <i class="bx bx-bell me-2"></i>
                <div class="me-auto fw-semibold">Message</div>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                {{ $message }}
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-10">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title fw-semibold mb-4">{{ __('AncillariesPage')['ancillariesFormCreateTitle'] }}</h5>
                    <form method="POST" class="forms-sample" action="{{ route('ancillaries-name.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="ancillaries_name" class="form-label">{{ __('AncillariesPage')['ancillariesFormInputName'] }}</label>
                            <input type="text" class="form-control" id="ancillaries_name" name="ancillaries_name">
                        </div>
                        <div class="mb-3">
                            <label for="ancillaries_type" class="form-label">Type</label>
                            <select id="ancillaries_type" name="ancillaries_type" class="form-select">
                                <option disabled selected>{{ __('AncillariesPage')['ancillariesFormTypeInputPlaceholder'] }}</option>
                                @foreach ($ancillaryTypes as $type)
                                    <option>{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="ancillaries_name_description" class="form-label">Description</label>
                            <input type="text" class="form-control" id="ancillaries_name_description"
                                name="ancillaries_name_description">
                        </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div id="sticky-div" style="position: fixed;">
                <div class="card">
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">{{ __('FormButton')['formCreateButtonText'] }}</button>
                        </form><hr>
                        <a href="{{ route('index') }}" class="btn btn-danger">{{ __('FormButton')['formCancelButtonText'] }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script>
        // Cache selectors outside callback for performance. 
        var $window = $(window),
            $stickyEl = $('#sticky-div'),
            elTop = $stickyEl.offset().top;

        $window.scroll(function() {
            $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
        });
    </script>
@endsection
