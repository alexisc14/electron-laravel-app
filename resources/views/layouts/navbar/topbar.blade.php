@php
    if (App::getLocale() == 'en') {
        Session::put('otherLang', 'fr');
        $langText = "FR";
    } else {
        Session::put('otherLang', 'en');
        $langText = "EN";
    }
@endphp

<header class="app-header">
    <nav class="navbar navbar-expand-lg navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item d-block d-xl-none">
                <a class="nav-link sidebartoggler nav-icon-hover" id="headerCollapse" href="javascript:void(0)">
                    <i class="ti ti-menu-2"></i>
                </a>
            </li>
        </ul>
        <div class="navbar-collapse justify-content-end px-0" id="navbarNav">
            <ul class="navbar-nav flex-row ms-auto align-items-center justify-content-end">
                <li class="nav-item dropdown">
                    <a class="nav-link nav-icon-hover" href="javascript:void(0)" id="drop2"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <img src='{{ asset('assets/img/countryflag/' . Session::get('localeLang') . '.png') }}'
                            alt="{{ Session::get('localeLang') }} lang" width="35" height="35" class="rounded-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-end dropdown-menu-animate-up" aria-labelledby="drop2">
                        <div class="message-body">
                            <form method="POST" action="{{ route('change.lang', Session::get('otherLang')) }}">
                                @csrf
                                <input type="text" name="lang" value="{{ Session::get('otherLang') }}" hidden>
                                <button type="submit" class="d-flex align-items-center gap-2 dropdown-item">
                                    <img src='{{ asset('assets/img/countryflag/' . Session::get('otherLang') . '.png') }}'
                                        alt="{{ Session::get('otherLang') }} lang" width="35" height="35" class="rounded-circle">
                                    {{ $langText }}
                                </button>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
