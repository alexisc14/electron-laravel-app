<aside class="left-sidebar">
    <div>
      <div class="brand-logo d-flex align-items-center justify-content-between">
        <a href="{{ route('index') }}" class="text-nowrap logo-img">
          <img src="{{ asset('assets/img/logos/logo_custom_properties-full-2.png') }}" width="180" style="margin-left:24%;margin-top:8%;width:65%;" alt="" />
        </a>
        <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
          <i class="ti ti-x fs-8"></i>
        </div>
      </div>
      <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
        <ul id="sidebarnav">
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Home</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('index') }}" aria-expanded="false">
              <span>
                <i class="ti ti-layout-dashboard"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['homepage'] }}</span>
            </a>
          </li>
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Test Number</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('test-number.create') }}" aria-expanded="false">
              <span>
                <i class="ti ti-test-pipe"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['testNumber'] }}</span>
            </a>
          </li>
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Ancillaries</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('ancillaries-name.create') }}" aria-expanded="false">
              <span>
                <i class="ti ti-luggage"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['ancillariesName'] }}</span>
            </a>
          </li>
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Fare Family</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('fare-family.create') }}" aria-expanded="false">
              <span>
                <i class="ti ti-flare"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['fareFamily'] }}</span>
            </a>
          </li>
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">{{ __('Navbar')['automatedTestsTitle'] }}</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('tnr.index') }}" aria-expanded="false">
              <span>
                <i class="ti ti-test-pipe"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['automatedTests'] }}</span>
            </a>
          </li>
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">{{ __('Navbar')['fileImportTitle'] }}</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="{{ route('import-custom-properties-file.index') }}" aria-expanded="false">
              <span>
                <i class="ti ti-file-upload"></i>
              </span>
              <span class="hide-menu">{{ __('Navbar')['fileImport'] }}</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
