@if($_SERVER['HTTP_USER_AGENT'] == "ELECTRON")
    <script>window.$ = window.jQuery = require('jquery');</script>
@else
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
@endif
<script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/sidebarmenu.js') }}"></script>
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/theme.js') }}"></script>