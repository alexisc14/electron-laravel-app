<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.header.header')
@yield('head-scripts')

<body>
    <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed">
        @include('layouts.navbar.navbar')
        <div class="body-wrapper">
            @include('layouts.navbar.topbar')
            <div class="container-fluid">
                @yield('content')
            </div>
            @include('layouts.footer.footer')
        </div>
    </div>
    @include('layouts.footer.footer-scripts')
    @yield('footer-scripts')
</body>

</html>
