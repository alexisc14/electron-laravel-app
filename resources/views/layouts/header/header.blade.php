    <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>{{ env('APP_NAME_TITLE') }} @yield('title')</title>

    <link rel="shortcut icon" href="{{ asset('assets/img/logos/logo_custom_properties-1.ico') }}">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" />

    <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css') }}" />

    @yield('header-scripts')
</head>
