@extends('layouts.master')
@section('title', "| " . __('PageTitle')['faremilyTitle'])
@section('header-scripts')
@endsection

@section('content')

    @if ($message = Session::get('message'))
        <div class="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-error" role="alert">
           {{ $message }}
        </div>
    @endif

    <div class="row">
        <div class="col-lg-10">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title fw-semibold mb-4">{{ __('FareFamilyPage')['fareFamilyFormCreateTitle'] }}</h5>
                    <form method="POST" class="forms-sample" action="{{ route('fare-family.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="cabin_type_name" class="form-label">{{ __('FareFamilyPage')['fareFamilyFormCabinTypeTitle'] }}</label>
                            <select id="cabin_type_name" name="cabin_type_name" class="form-select">
                                <option>{{ __('FareFamilyPage')['fareFamilyFormCabinTypeNamePlaceholder'] }}</option>
                                <option>ECONOMY</option>
                                <option>PREMIUM</option>
                                <option>BUSINESS</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="fare_family_name" class="form-label">{{ __('FareFamilyPage')['fareFamilyFormInputName'] }}</label>
                            <input type="text" class="form-control" id="fare_family_name" name="fare_family_name">
                        </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div id="sticky-div" style="position: fixed;">
                <div class="card">
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">{{ __('FormButton')['formCreateButtonText'] }}</button>
                        </form><hr>
                        <a href="{{ route('index') }}" class="btn btn-danger">{{ __('FormButton')['formCancelButtonText'] }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script>
        // Cache selectors outside callback for performance. 
        var $window = $(window),
            $stickyEl = $('#sticky-div'),
            elTop = $stickyEl.offset().top;

        $window.scroll(function() {
            $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
        });
    </script>
@endsection
