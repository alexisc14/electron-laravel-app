@extends('layouts.master')
@section('title', "| " . __('PageTitle')['homepageTitle'])
@section('header-scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap5.css" />
@endsection
@section('content')

    @if ($message = Session::get('message'))
        <div class="alert alert-success" id="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger" id="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif

    <div class="mb-2" id="exportMessage"></div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        @if (count($testCases) > 0)
                            <div class="mb-3 mb-sm-0">
                                <h5 class="card-title fw-semibold">Test Numbers
                                    <a class="text-dark" href="{{ route('test-number.create') }}">
                                        <i data-bs-toggle="tooltip" data-bs-placement="right"
                                            data-bs-original-title="{{ __('Homepage')['testNumbersTableAddNewTestNumberTitle'] }}"
                                            class="ti ti-circle-plus me-2 fs-4"></i>
                                    </a>
                                    <button style="border: none;background: transparent;" id="findLastUptdTestNumber"
                                        class="text-dark"
                                        onclick="searchDatatable('{{ substr($lastUpdatedTestCase->correlation_id, 0, 36) }}...')">
                                        <i data-bs-toggle="tooltip" data-bs-placement="right"
                                            data-bs-original-title="{{ __('Homepage')['testNumbersTableFindLastUptdTestNumber'] }} {{ $lastUpdatedTestCase->TestNumber }}"
                                            class="ti ti-reload me-2 fs-4"></i>
                                    </button>
                                    <button style="display:none;border:none;background:transparent;"
                                        id="clearDataTableSearchBtn" class="text-dark"
                                        onclick="searchDatatable({{ $lastUpdatedTestCase->TestNumber }})">
                                        <i data-bs-toggle="tooltip" data-bs-placement="right"
                                            data-bs-original-title="{{ __('Homepage')['testNumbersTableClearDataTableSearchBtn'] }}"
                                            class="ti ti-x me-2 fs-4"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="exportAsCSVBtnDiv" style="margin-left:59.5%;" class="mb-3 mb-sm-0">
                                <button id="exportAsCSVBtn" type="button"
                                    onclick="exportTestsNumberConfirm('{{ __('Homepage')['testNumbersTableExportWarningMessage'] }} {{ count($testCases) - 1 }} tests ?')"
                                    data-bs-toggle="tooltip" data-bs-placement="right"
                                    data-bs-original-title="{{ __('Homepage')['testNumbersTableExportBtnTooltipMessage1'] }} {{ count($testCases) - 1 }} {{ __('Homepage')['testNumbersTableExportBtnTooltipMessage2'] }}"
                                    class="btn btn-success">
                                    {{ __('Homepage')['testNumbersTableExportButton'] }}
                                </button>
                            </div>
                            <div class="mb-3 mb-sm-0">
                                <form method="POST" action="{{ route('destroy.all.tn') }}">
                                    @csrf
                                    <button
                                        onclick="return confirm('{{ __('Homepage')['testNumbersTableDeleteWarningMessage1'] }} {{ count($testCases) - 1 }} {{ __('Homepage')['testNumbersTableDeleteWarningMessage2'] }}"
                                        type="submit" class="btn btn-danger">
                                        {{ __('Homepage')['testNumbersTableDeleteAllButton'] }}
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="testCasesTable" class="table table-striped">
                                    <thead class="text-dark fs-4" style="text-align:center;">
                                        <tr>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">TestNumber</h6>
                                            </th>
                                            <th class="border-bottom-0">
                                                <h6 class="fw-semibold mb-0">CID <span id="CIDInfo"><i>({{ __('Homepage')['testNumbersTableCIDColumnSpanText'] }})</i><span></h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Airline</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">IATA</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">PCC</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">PAX</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Actions</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($testCases as $data)
                                            <tr>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <a href="{{ route('test-number.show', $data->id) }}"
                                                        class="text-primary edit">
                                                        {{ $data->TestNumber }}
                                                    </a>
                                                </td>
                                                <td class="border-bottom-0">
                                                    <p class="mb-0 fw-normal" data-bs-toggle="tooltip"
                                                        data-bs-placement="left"
                                                        data-bs-original-title="{{ $data->correlation_id }}">
                                                        {{ substr($data->correlation_id, 0, 36) }}...</p>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <p class="mb-0 fw-normal">{{ $data->airline }}</p>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    @if ($data->airline == 'AF')
                                                        <div>
                                                            <span
                                                                class="badge bg-primary rounded-3 fw-semibold">{{ $data->iata_agency }}</span>
                                                        </div>
                                                    @else
                                                        <div>
                                                            <span
                                                                class="badge bg-warning rounded-3 fw-semibold">{{ $data->iata_agency }}</span>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <p class="mb-0 fw-normal">{{ $data->pseudo_city }}</p>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <p class="mb-0 fw-normal">{{ $data->pax }}
                                                    <p>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <div class="action-btn">
                                                        <form method="POST"
                                                            action="{{ route('test-number.destroy', $data->id) }}">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit"
                                                                onclick="return confirm('{{ __('Homepage')['testNumbersTableDeleteSingleTestNumberMessage'] }} {{ $data->TestNumber }} ?')"
                                                                class="text-danger delete ms-2"
                                                                style="border:none;padding-right:30%;background-color: transparent;">
                                                                <i class="ti ti-trash fs-5"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 d-flex align-items-strech">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div class="mb-3 mb-sm-0">
                            <h5 class="card-title fw-semibold">Ancillaries <a class="text-dark"
                                    href="{{ route('ancillaries-name.create') }}"><i data-bs-toggle="tooltip"
                                        data-bs-placement="right" data-bs-original-title="{{ __('Homepage')['ancillariesTableTooltipCreateMessage'] }}"
                                        class="ti ti-circle-plus me-2 fs-4"></i></a></h5>
                        </div>
                    </div>
                    <div>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="ancillariesNameTable" class="table table-striped">
                                    <thead class="text-dark fs-4" style="text-align:center;">
                                        <tr>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">{{ __('Homepage')['ancillariesTableAncillaryNameColumn'] }}</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Type</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Description</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Actions</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ancillariesName as $data)
                                            <tr>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <h6 class="fw-semibold mb-0">{{ $data->name ?? "" }}</h6>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <h6 class="fw-semibold mb-1">{{ $data->type ?? "" }}</h6>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <h6 class="fw-semibold mb-1">{{ $data->name_description ?? "" }}</h6>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <div class="action-btn">
                                                        <a href="{{ route('ancillaries-name.show', $data->id) }}"
                                                            class="text-primary edit"
                                                            style="float: left; padding-left: 20%;">
                                                            <i class="ti ti-eye fs-5"></i>
                                                        </a>
                                                        <form method="POST"
                                                            action="{{ route('ancillaries-name.destroy', $data->id) }}">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit"
                                                                onclick="return confirm('{{ __('Homepage')['ancillariesTableDeleteWarningMessage'] }} {{ $data->name }} ?')"
                                                                class="text-danger delete ms-2"
                                                                style="border:none;padding-right:30%;background-color: transparent;">
                                                                <i class="ti ti-trash fs-5"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 d-flex align-items-strech">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div class="mb-3 mb-sm-0">
                            <h5 class="card-title fw-semibold">Fare Family <a class="text-dark"
                                    href="{{ route('fare-family.create') }}"><i data-bs-toggle="tooltip"
                                        data-bs-placement="right" data-bs-original-title="{{ __('Homepage')['fareFamilyTableTooltipCreateMessage'] }}"
                                        class="ti ti-circle-plus me-2 fs-4"></i></a></h5>
                        </div>
                    </div>
                    <div>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="fareFamilyTable" class="table table-striped">
                                    <thead class="text-dark fs-4" style="text-align:center;">
                                        <tr>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">{{ __('Homepage')['fareFamilyTableCabinColumnName'] }}</h6>
                                            </th>
                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Fare Family</h6>
                                            </th>

                                            <th class="border-bottom-0" style="text-align:center;">
                                                <h6 class="fw-semibold mb-0">Actions</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($fareFamilies as $data)
                                            <tr>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <h6 class="fw-semibold mb-0">{{ $data->cabin_type }}</h6>
                                                </td>
                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <h6 class="fw-semibold mb-1">{{ $data->fare_family_name }}</h6>
                                                </td>

                                                <td class="border-bottom-0" style="text-align:center;">
                                                    <div class="action-btn">
                                                        <a href="{{ route('fare-family.show', $data->id) }}"
                                                            class="text-primary edit"
                                                            style="float: left;padding-left: 35%;">
                                                            <i class="ti ti-eye fs-5"></i>
                                                        </a>
                                                        <form method="POST"
                                                            action="{{ route('fare-family.destroy', $data->id) }}">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit"
                                                                onclick="return confirm('{{ __('Homepage')['fareFamilyTableDeleteWarningMessage'] }} {{ $data->fare_family_name }} ?')"
                                                                class="text-danger delete ms-2"
                                                                style="border:none;padding-right:30%;background-color: transparent;">
                                                                <i class="ti ti-trash fs-5"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/2.0.8/sorting/natural.js"></script>
    <script>
        function searchDatatable(val) {
            $('#clearDataTableSearchBtn').css({
                'display': ''
            });
            $('#exportAsCSVBtnDiv').css({
                'margin-left': '57%'
            });
            $('#testCasesTable').DataTable().search(val).draw();
        }

        function exportTestsNumberConfirm(message) {
            return confirm(message);
        }

        $(document).ready(function() {

            let testCasesTable = new DataTable('#testCasesTable', {
                responsive: true,
                language: {
                    url: "{{ __('Language')['datatable'] }}"
                }
            });

            $('#clearDataTableSearchBtn').on('click', function() {
                $('#testCasesTable').DataTable().search("").draw();
                $('#clearDataTableSearchBtn').css({
                    'display': 'none'
                });
                $('#exportAsCSVBtnDiv').css({
                    'margin-left': '59.5%'
                });
            });

            $("#CIDInfo").css({
                "font-size": '75%'
            });

            let ancillariesNameTable = new DataTable('#ancillariesNameTable', {
                responsive: true,
                language: {
                    url: "{{ __('Language')['datatable'] }}"
                }
            });

            let fareFamilyTable = new DataTable('#fareFamilyTable', {
                responsive: true,
                language: {
                    url: "{{ __('Language')['datatable'] }}"
                }
            });


            $(".alert").delay(4000).slideUp(200, function() {
                $("#alert alert-success").fadeTo(2000, 500).slideUp(500, function() {
                    $("#alert alert-success").slideUp(500);
                });

                $("#alert alert-danger").fadeTo(2000, 500).slideUp(500, function() {
                    $("#alert alert-danger").slideUp(500);
                });
            });


            $('#exportAsCSVBtn').click(function(event) {
                event.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('export.as.csv') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(data) {
                        $("#exportMessage").css({
                            'display': 'flex'
                        });
                        $("#exportMessage").html('<div class="alert alert-success text-success alert-dismissible fade show" role="alert" style="font-weight: bold;"> \
                                                                                <div class="d-flex align-items-center text-success "> \
                                                                                <i class="ti ti-circle-check me-2 fs-4"></i> \
                                                                                ' + data.message + ' \
                                                                                </div> \
                                                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>\
                                                                            </div>')

                        setTimeout(function() {
                            $("#exportMessage").slideUp(500);
                        }, 4000);

                    },
                    error: function(error) {
                        let message = ""
                        if (error.responseJSON.message.includes(
                                "Resource temporarily unavailable")) {
                            message =
                                "{{ __('Homepage')['testNumbersTableExportErrorMessage'] }}"
                        } else {
                            message = error.responseJSON.message
                        }
                        $("#exportMessage").css({
                            'display': 'flex'
                        });
                        $("#exportMessage").html('<div class="alert alert-danger text-danger alert-dismissible fade show" role="alert" style="font-weight: bold;"> \
                                                                                <div class="d-flex align-items-center text-danger "> \
                                                                                <i class="ti ti-exclamation-circle me-2 fs-4"></i> \
                                                                                ' + message + ' \
                                                                                </div> \
                                                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>\
                                                                            </div>')

                        setTimeout(function() {
                            $("#exportMessage").slideUp(500);
                        }, 4000);
                    }
                })
            })

        })
    </script>
@endsection
