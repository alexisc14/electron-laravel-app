@extends('layouts.master')
@section('title', "| " . __('PageTitle')['autoTestsTitle'])
@section('header-scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap5.css" />
@endsection
@section('content')

    <div id="alertNotif">
        @if ($message = Session::get('message'))
            <div class="alert alert-success" id="alert alert-success" role="alert">
                {{ $message }}
            </div>
        @elseif($message = Session::get('error'))
            <div class="alert alert-danger" id="alert alert-danger" role="alert">
                {{ $message }}
            </div>
        @endif
    </div>

    <div class="mb-2" id="exportMessage"></div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div class="mb-3 mb-sm-0">
                            <h5 class="card-title fw-semibold">Use Cases</h5>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="testCasesTable" class="table table-striped">
                                <thead class="text-dark fs-4" style="text-align:center;">
                                    <tr>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">TestNumber</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0" style="">SoapUI Test Case</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Airline</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">IATA</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">PCC</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">PAX</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Actions</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($testCases as $data)
                                        <tr>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->TestNumber }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="">
                                                <p class="mb-0 fw-normal">{{ $data->UseCase }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->airline }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                @if ($data->airline == 'AF')
                                                    <div>
                                                        <span
                                                            class="badge bg-primary rounded-3 fw-semibold">{{ $data->iata_agency }}</span>
                                                    </div>
                                                @else
                                                    <div>
                                                        <span
                                                            class="badge bg-warning rounded-3 fw-semibold">{{ $data->iata_agency }}</span>
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->pseudo_city }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->pax }}
                                                <p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <div class="action-btn">
                                                    <button data-bs-toggle="tooltip" data-bs-placement="left"
                                                        data-bs-original-title="{{ __('AutomatedTesting')['automatedTestsUseCaseTableRunBtnText'] }} : {{ $data->UseCase }}"
                                                        onclick='runTestCase({{ $data->TestNumber }})' id="btnRunTestCase"
                                                        class="text-success"
                                                        style="float:left;padding-left:30%;border:none;background-color: transparent;">
                                                        <i class="ti ti-player-play fs-5"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div class="mb-3 mb-sm-0">
                            <h5 class="card-title fw-semibold">Tests Auto Executions</h5>
                            <div id="loadingExecTNR" role="status" style="margin-top:10%;"></div>
                            <span id="loadingExecTNRText" class="visually-hidden"><i>{{ __('AutomatedTesting')['automatedTestsExecutedTableLoadingExecMessage'] }}</i></span>
                        </div>
                        <div class="mb-3 mb-sm-0">
                            <form method="POST" action="{{ route('destroy.all.tnr') }}">
                                @csrf
                                <button title="{{ __('AutomatedTesting')['automatedTestsExecutedTableDeleteHistoryBtnTitle'] }}"
                                    onclick="return confirm('{{ __('AutomatedTesting')['automatedTestsExecutedTableDeleteHistoryWarningMessage'] }}')"
                                    type="submit" class="btn btn-danger">{{ __('AutomatedTesting')['automatedTestsExecutedTableDeleteHistoryBtnText'] }}</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-12" id="testReload">
                        <div class="table-responsive">
                            <table id="testCasesTNRTable" class="table table-striped">
                                <thead class="text-dark fs-4" style="text-align:center;">
                                    <tr>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0" style="text-align:center;">SoapUI Test Case</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">CID <span id="CIDInfo"><i>({{ __('AutomatedTesting')['automatedTestsExecutedTableCIDColumnSpanText'] }})</i><span></h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Status</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Success</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Regression</h6>
                                        </th>
                                        <th class="border-bottom-0" style="text-align:center;">
                                            <h6 class="fw-semibold mb-0">Date exec</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($tnrs as $data)
                                        <tr>
                                            <td class="border-bottom-0" style="text-align:justify;">
                                                @if ($data->success != 0)
                                                    <a class="text-danger" href="{{ route('tnr.show', $data->id) }}">
                                                        <p class="mb-0 fw-normal">{{ $data->useCase }}</p>
                                                    </a>
                                                @else
                                                    <a class="text-normal" href="{{ route('tnr.show', $data->id) }}">
                                                        <p class="mb-0 fw-normal">{{ $data->useCase }}</p>
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="border-bottom-0">
                                                <p class="mb-0 fw-normal" data-bs-toggle="tooltip"
                                                    data-bs-placement="left"
                                                    data-bs-original-title="{{ $data->cid }}">
                                                    {{ substr($data->cid, 0, 36) }}...</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->status }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                @if ($data->success == 0)
                                                    <span class="text-success">
                                                        <i class="ti ti-checks" style="font-size:1.6em;"></i>
                                                    </span>
                                                @else
                                                    <span class="text-danger" data-bs-toggle="tooltip"
                                                        data-bs-placement="left"
                                                        data-bs-original-title="{{ __('AutomatedTesting')['automatedTestsExecutedTableTooltipTitleErrorText'] }}">
                                                        <i class="ti ti-alert-circle" style="font-size:1.6em;"></i>
                                                    </span>
                                                @endif
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">{{ $data->isRegression ?? 'N/A' }}</p>
                                            </td>
                                            <td class="border-bottom-0" style="text-align:center;">
                                                <p class="mb-0 fw-normal">
                                                    {{ date('d/m/Y - H:i:s', strtotime($data->updated_at)) ?? 'N/A' }}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/2.0.8/sorting/natural.js"></script>
    <script>
        $(document).ready(function() {
            let dataTableLang = 'https://cdn.datatables.net/plug-ins/2.0.8/i18n/fr-FR.json'

            let testCasesTable = new DataTable('#testCasesTable', {
                responsive: true,
                language: {
                    url: "{{ __('Language')['datatable'] }}"
                }
            });
            let testCasesTNRTable = new DataTable('#testCasesTNRTable', {
                responsive: true,
                "order": [
                    [5, "desc"]
                ],
                language: {
                    url: "{{ __('Language')['datatable'] }}"
                }
            });
            $("#CIDInfo").css({
                "font-size": '75%'
            });
        });

        function searchDatatable(val) {
            $('#testCasesTable').DataTable().search(val).draw();
        }

        function runTestCaseTest() {
            $("#btnRunTestCase").on('click', function() {
                console.log("ici")
                $("#loadingExecTNR").addClass('spinner-border spinner-border-sm text-primary')
                $("#loadingExecTNRText").removeClass('visually-hidden')
            })

            setTimeout(() => {
                $("#loadingExecTNR").removeClass('spinner-border spinner-border-sm text-primary')
                $("#loadingExecTNRText").addClass('visually-hidden')
            }, 5000);
        }

        function runTestCase(testNumberID) {
            $.ajax({
                type: "POST",
                url: "{{ route('tnr.store') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "testNumber": testNumberID
                },
                beforeSend: function() {
                    $("#loadingExecTNR").addClass('spinner-border spinner-border-sm text-primary')
                    $("#loadingExecTNRText").removeClass('visually-hidden')
                },
                error: function(data) {
                    $("#loadingExecTNR").removeClass('spinner-border spinner-border-sm text-primary')
                    $("#loadingExecTNRText").addClass('visually-hidden')
                    if (data.responseJSON.message) {
                        $("#alertNotif").html(
                            '<div class="alert alert-danger" id="alert alert-danger" role="alert">' + data
                            .responseJSON.message + '</div>')
                        $("#testReload").load(location.href + " #testCasesTNRTable", function() {
                            $('#testCasesTNRTable').DataTable({
                                responsive: true,
                                "order": [
                                    [5, "desc"]
                                ],
                                language: {
                                    url: "{{ __('Language')['datatable'] }}"
                                }
                            })
                        });
                    }
                },
                success: function(data) {
                    $("#loadingExecTNR").removeClass('spinner-border spinner-border-sm text-primary')
                    $("#loadingExecTNRText").addClass('visually-hidden')
                    if (data.message) {
                        $("#alertNotif").html(
                            '<div class="alert alert-success" id="alert alert-success" role="alert">' + data
                            .message + '</div>')
                        $("#testReload").load(location.href + " #testCasesTNRTable", function() {
                            $('#testCasesTNRTable').DataTable({
                                responsive: true,
                                "order": [
                                    [5, "desc"]
                                ],
                                language: {
                                    url: "{{ __('Language')['datatable'] }}"
                                }
                            })
                        });
                    } else {
                        $("#alertNotif").html(
                            '<div class="alert alert-danger" id="alert alert-danger" role="alert">' + data
                            .error + '</div>')
                        $("#testReload").load(location.href + " #testCasesTNRTable", function() {
                            $('#testCasesTNRTable').DataTable({
                                responsive: true,
                                "order": [
                                    [5, "desc"]
                                ],
                                language: {
                                    url: "{{ __('Language')['datatable'] }}"
                                }
                            })
                        });
                    }
                }
            })
        }
    </script>
@endsection
