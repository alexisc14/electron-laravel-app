@extends('layouts.master')
@section('title', "| " . __('PageTitle')['autoTestsSelectedTestTitle'] ." $testCase->useCase")
@section('header-scripts')
@endsection
@section('content')

    <div id="alertNotif">
        @if ($message = Session::get('message'))
            <div class="alert alert-success" id="alert alert-success" role="alert">
                {{ $message }}
            </div>
        @elseif($message = Session::get('error'))
            <div class="alert alert-danger" id="alert alert-danger" role="alert">
                {{ $message }}
            </div>
        @endif
    </div>

    <div class="mb-2" id="exportMessage"></div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-body">
                <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                    <div class="mb-3 mb-sm-0">
                        @php
                            $statusColor = $testCase->status == 'ERROR' ? 'text-danger' : 'text-success';
                        @endphp
                        <h5 class="card-title fw-semibold">Exec #{{ $testCase->id }} : "{{ $testCase->useCase }}"
                            {{ __('AutomatedTesting')['automatedTestsExecutedSelectedTestTitlePart'] }} : <span class="{{ $statusColor }}">{{ $testCase->status }}</span></h5>
                    </div>
                </div>
                <br>
                <hr>
                <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                    <div class="mb-1 mb-sm-0">
                        <b>CLAT :</b> <a href="https://clat-como-ute.klm.com/#/messages?correlationId={{ $testCase->cid }}"
                            title="{{ __('AutomatedTesting')['automatedTestsExecutedSelectedTestCLATRedirectTitle'] }}" target="_blank">{{ $testCase->cid }}</a>
                    </div>
                </div>
                <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                    <div class="mb-3 mb-sm-0">
                        <b>COMO :</b> <a
                            href="https://como-cae.af-klm.com/kibana/app/dashboards#/view/AXez8jL1GTkse4i4SPuC?_g=(filters:!((query:(match_phrase:(correlationId:{{ $testCase->cid }})))),refreshInterval:(pause:!f,value:300000),time:(from:now-7d%2Fd,to:now))"
                            title="{{ __('AutomatedTesting')['automatedTestsExecutedSelectedTestCOMORedirectTitle'] }}" target="_blank">{{ $testCase->cid }}</a>
                    </div>
                </div>
                <hr>
                <br>
                @if ($publicLogsPathFailedFolder !== '')
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div id="testResult">
                            <div id="publicLogsPathFailedFolder"
                                data-php-publicLogsPathFailedFolder="{{ $publicLogsPathFailedFolder }}"></div>
                            <div id="failedFileName" data-php-failedFileName="{{ $failedFileName }}"></div>
                            <div id="formattedTestCase" data-php-formattedTestCase="{{ $formattedTestCase }}"></div>
                            <div id="filecontent"
                                data-php-filecontent="{{ file_get_contents(public_path("tnrexecs/$formattedTestCase/$failedFileName")) }}">
                            </div>
                            <pre id="testResultContent" class='brush: xml;'></pre>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>


@endsection
@section('footer-scripts')
    <script>
        $(document).ready(function() {
            var publicLogsPathFailedFolder = document.getElementById('publicLogsPathFailedFolder').getAttribute(
                'data-php-publicLogsPathFailedFolder');
            var failedFileName = document.getElementById('failedFileName').getAttribute('data-php-failedFileName');
            var formattedTestCase = document.getElementById('formattedTestCase').getAttribute(
                'data-php-formattedTestCase');
            var contentPath = document.getElementById('filecontent').getAttribute('data-php-filecontent');
            const content = document.querySelector("#testResultContent");
            const reader = new FileReader();
            var file = new Blob([contentPath], {
                type: 'text/plain'
            });

            reader.addEventListener(
                "load",
                () => {
                    // this will then display a text file
                    content.innerText = reader.result;
                },
                false,
            );

            if (file) {
                reader.readAsText(file);
            }
        })
    </script>
@endsection
