<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TestCase;
use App\Models\FareFamily;
use App\Models\AncillariesName;
use App\Http\Controllers\ExportAsCSVController;

class TestCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $fareFamilies = FareFamily::orderBy('cabin_type','ASC')->get();
        $ancillariesName = AncillariesName::orderBy('id','ASC')->get();
        return view('testcase.create', compact('fareFamilies','ancillariesName'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $isTestNumber = TestCase::where('TestNumber',$request->TestNumber)->first();
        if ($isTestNumber){
            $messageTemplate = __('Controllers')['TestCase']['Functions']['storeCustomErrorMessage1'];
            $message = str_replace(":testNumber", $request->TestNumber, $messageTemplate);
            return redirect('/test-number/create', 301)->with(compact('error'));
        }

        if ($request->TestNumber == "1" || $request->TestNumber == "2"){
            $messageTemplate = __('Controllers')['TestCase']['Functions']['storeCustomErrorMessage2'];
            $message = str_replace(":testNumber", $request->TestNumber, $messageTemplate);
            return redirect('/test-number/create', 301)->with(compact('error'));
        }

        TestCase::firstOrCreate([
            'TestNumber' => $request->TestNumber,
            'correlation_id' => $request->correlation_id,
            'airline' => $request->airline,
            'pseudo_city' => $request->pseudo_city,
            'iata_agency' => $request->iata_agency,
            'travel_agency_name' => $request->travel_agency_name,
            'cabin_type_Name' => $request->cabin_type_Name,
            'StrictSearchCabin' => $request->StrictSearchCabin,
            'StrictSearchFF' => $request->StrictSearchFF,
            'bundle_seats' => $request->bundle_seats,
            'GST_Tax' => $request->GST_Tax,
            'upsell' => $request->upsell,
            'bundleSeatAtUpsell' => $request->bundleSeatAtUpsell,
            'countrycode' => $request->countrycode,
            'pax' => $request->pax,
            'paymentMethod' => $request->paymentMethod,
            'recognition' => $request->recognition,
            'MultiplePricingProgram' => $request->MultiplePricingProgram,
            'pricing_program' => $request->pricing_program,
            'account_code' => $request->account_code,
            'OIN' => $request->OIN,
            'OnD' => $request->OnD,
            'OnDRebooking' => $request->OnDRebooking,
            'Date' => $request->Date,
            'DateRebooking' => $request->DateRebooking,
            'order_id' => $request->order_id,
            'private_fare' => $request->private_fare,
            'Bag' => $request->Bag,
            'BagRebook' => $request->BagRebook,
            'Seat' => $request->Seat,
            'SeatRebook' => $request->SeatRebook,
            'FareFamilyRebook' => $request->FareFamilyRebook,
            'APIS' => $request->APIS,
            'APIS_type' => $request->APIS_type,
            'APIS_update_pax' => $request->APIS_update_pax,
            'APIS_pax_titlename' => $request->APIS_pax_titlename,
            'APIS_after_create' => $request->APIS_after_create,
            'LoyaltyProgramUpdatePax' => $request->LoyaltyProgramUpdatePax,
            'LoyaltyProgramAfterBooking' => $request->LoyaltyProgramAfterBooking,
            'LoyaltyProgram' => $request->LoyaltyProgram,
            'LoyaltyProgramTierName' => $request->LoyaltyProgramTierName,
            'LP_programName' => $request->LP_programName,
            'LP_account' => $request->LP_account,
            'LP_civility' => $request->LP_civility,
            'LP_surname' => $request->LP_surname,
            'LP_givenname' => $request->LP_givenname,
            'split_pax_to_update' => $request->SplitUpdatePax,
            'created_at' => now()
        ]);

        $messageTemplate = __('Controllers')['TestCase']['Functions']['store'];
        $message = str_replace(":testNumber", $request->TestNumber, $messageTemplate);
        return redirect('/', 301)->with(compact('message'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $fareFamilies = FareFamily::orderBy('cabin_type','ASC')->get();
        $ancillariesName = AncillariesName::orderBy('id','ASC')->get();
        $testCase = TestCase::find($id);
        return view('testcase.show', compact('testCase', 'fareFamilies', 'ancillariesName'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $testCase = TestCase::find($id);
        return view('testcase.edit', compact('testCase'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $testCase = TestCase::find($id);

        $testCase->correlation_id = $request->correlation_id;
        $testCase->airline = $request->airline;
        $testCase->pseudo_city = $request->pseudo_city;
        $testCase->iata_agency = $request->iata_agency;
        $testCase->travel_agency_name = $request->travel_agency_name;
        $testCase->cabin_type_Name = $request->cabin_type_Name;
        $testCase->StrictSearchCabin = $request->StrictSearchCabin;
        $testCase->StrictSearchFF = $request->StrictSearchFF;
        $testCase->bundle_seats = $request->bundle_seats;
        $testCase->GST_Tax = $request->GST_Tax;
        $testCase->upsell = $request->upsell;
        $testCase->bundleSeatAtUpsell = $request->bundleSeatAtUpsell;
        $testCase->countrycode = $request->countrycode;
        $testCase->pax = $request->pax;
        $testCase->paymentMethod = $request->paymentMethod;
        $testCase->recognition = $request->recognition;
        $testCase->MultiplePricingProgram = $request->MultiplePricingProgram;
        $testCase->pricing_program = $request->pricing_program;
        $testCase->account_code = $request->account_code;
        $testCase->OIN = $request->OIN;
        $testCase->OnD = $request->OnD;
        $testCase->OnDRebooking = $request->OnDRebooking;
        $testCase->Date = $request->Date;
        $testCase->DateRebooking = $request->DateRebooking;
        $testCase->order_id = $request->order_id;
        $testCase->private_fare = $request->private_fare;
        $testCase->Bag = $request->Bag;
        $testCase->BagRebook = $request->BagRebook;
        $testCase->Seat = $request->Seat;
        $testCase->SeatRebook = $request->SeatRebook;
        $testCase->FareFamilyRebook = $request->FareFamilyRebook;
        $testCase->APIS = $request->APIS;
        $testCase->APIS_type = $request->APIS_type;
        $testCase->APIS_update_pax = $request->APIS_update_pax;
        $testCase->APIS_pax_titlename = $request->APIS_pax_titlename;
        $testCase->APIS_after_create = $request->APIS_after_create;
        $testCase->LoyaltyProgramUpdatePax = $request->LoyaltyProgramUpdatePax;
        $testCase->LoyaltyProgramAfterBooking = $request->LoyaltyProgramAfterBooking;
        $testCase->LoyaltyProgram = $request->LoyaltyProgram;
        $testCase->LoyaltyProgramTierName = $request->LoyaltyProgramTierName;
        $testCase->LP_programName = $request->LP_programName;
        $testCase->LP_account = $request->LP_account;
        $testCase->LP_civility = $request->LP_civility;
        $testCase->LP_surname = $request->LP_surname;
        $testCase->LP_givenname = $request->LP_givenname;
        $testCase->split_pax_to_update = $request->SplitUpdatePax;
        $testCase->updated_at = now();

        $testCase->update();

        $export = ExportAsCSVController::export();
        $exportDecoded = json_decode($export->content());
        if ($exportDecoded->status == 200){
            $exportMessage = __('Controllers')['TestCase']['Functions']['updateExportMessage'];
        }

        $messageTemplate = __('Controllers')['TestCase']['Functions']['update'] . " $exportMessage";
        $message = str_replace(":testNumber", $testCase->TestNumber, $messageTemplate);
        return redirect("/", 301)->with(compact('message'));
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $testCase = TestCase::find($id);
        $messageTemplate = __('Controllers')['TestCase']['Functions']['destroy'];
        $message = str_replace(":testNumber", $testCase->TestNumber, $messageTemplate);
        $testCase->delete();
        return redirect('/', 301)->with(compact('message'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyAll()
    {
        /** Delete the entire table + reset the auto-incrementing IDs to zero */
        TestCase::truncate();
        $message = __('Controllers')['TestCase']['Functions']['destroyAll'];
        return redirect('/', 301)->with(compact('message'));
    }
}
