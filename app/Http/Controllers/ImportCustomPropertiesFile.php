<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TestCase;
use App\Models\TestCaseTNR;

use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportCustomPropertiesFile extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('custompropertiesfile.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $modelID = $request->modelID;
        $modelArray = ["TestCase" => 1, "TestCaseTNR" => 2];
        $modelName = array_search($modelID, $modelArray);
        // Define specific model to use according to modelID received
        $model = app("App\Models\\$modelName");
        
        /** Delete the entire table + reset the auto-incrementing IDs to zero */
        $model::truncate();

        $reader = IOFactory::createReader('Csv');

        // Define ";" character as delimiter
        $reader->setDelimiter(';');
        // Read first sheet
        $reader->setSheetIndex(0); 

        $csvFile = request()->file("csvFile");
        
        $spreadsheet = $reader->load($csvFile);
        $sheet = $spreadsheet->getActiveSheet();
        $customPropertiesData = $sheet->toArray();
        $insertDataArr = [];
        $nbRow = 0;
        foreach ($customPropertiesData as $row) {
            $row = array_values($row);
            $testNumber = $row[0];
            if ($testNumber !== null && $testNumber !== "TestNumber" && $testNumber !== ""){
                $insertDataArr['TestNumber'] = $testNumber;
                $insertDataArr['correlation_id'] = $row[11];
                $insertDataArr['airline'] = $row[12];
                $insertDataArr['pseudo_city'] = $row[13];
                $insertDataArr['iata_agency'] = $row[14];
                $insertDataArr['travel_agency_name'] = $row[15];
                $insertDataArr['cabin_type_Name'] = $row[16];
                $insertDataArr['StrictSearchCabin'] = $row[17];
                $insertDataArr['StrictSearchFF'] = $row[18];
                $insertDataArr['bundle_seats'] = $row[19];
                $insertDataArr['GST_Tax'] = $row[20];
                $insertDataArr['upsell'] = $row[21];
                $insertDataArr['bundleSeatAtUpsell'] = $row[22];
                $insertDataArr['countrycode'] = $row[23];
                $insertDataArr['pax'] = $row[24];
                $insertDataArr['paymentMethod'] = $row[25];
                $insertDataArr['recognition'] = $row[26];
                $insertDataArr['MultiplePricingProgram'] = $row[27];
                $insertDataArr['pricing_program'] = $row[28];
                $insertDataArr['account_code'] = $row[29];
                $insertDataArr['OIN'] = $row[30];
                $insertDataArr['OnD'] = $row[31];
                $insertDataArr['OnDRebooking'] = $row[32];
                $insertDataArr['Date'] = $row[33];
                $insertDataArr['DateRebooking'] = $row[34];
                $insertDataArr['order_id'] = $row[35];
                $insertDataArr['private_fare'] = $row[36];
                $insertDataArr['Bag'] = $row[37];
                $insertDataArr['BagRebook'] = $row[38];
                $insertDataArr['Seat'] = $row[39];
                $insertDataArr['SeatRebook'] = $row[40];
                $insertDataArr['FareFamilyRebook'] = $row[41];
                $insertDataArr['APIS'] = $row[42];
                $insertDataArr['APIS_type'] = $row[43];
                $insertDataArr['APIS_update_pax'] = $row[44];
                $insertDataArr['APIS_pax_titlename'] = $row[45];
                $insertDataArr['APIS_after_create'] = $row[46];
                $insertDataArr['LoyaltyProgramUpdatePax'] = $row[47];
                $insertDataArr['LoyaltyProgramAfterBooking'] = $row[48];
                $insertDataArr['LoyaltyProgram'] = $row[49];
                $insertDataArr['LoyaltyProgramStep'] = $row[50];
                $insertDataArr['LoyaltyProgramTierName'] = $row[51];
                $insertDataArr['LP_programName'] = $row[52];
                $insertDataArr['LP_account'] = $row[53];
                $insertDataArr['LP_civility'] = $row[54];
                $insertDataArr['LP_surname'] = $row[55];
                $insertDataArr['LP_givenname'] = $row[56];
                if ($modelID == 2){
                    $insertDataArr['UseCase'] = $row[57];
                }
                $insertDataArr['created_at'] = now();
                
                $model::firstOrCreate($insertDataArr);
                $nbRow++;
            }
        }

        $s = $nbRow > 1 ? "s" : "";
        if ($modelName == "TestCaseTNR"){
            $messageTemplate = __('Controllers')['ImportCustomProperties']['Functions']['storeTNRMessage'];
            $message = str_replace([":nbTest", ":s"], [$nbRow, $s], $messageTemplate);
            return redirect('/tnr', 301)->with(compact('message'));
        } else {
            $messageTemplate = __('Controllers')['ImportCustomProperties']['Functions']['store'];
            $message = str_replace([":nbTest", ":s"], [$nbRow, $s], $messageTemplate);
            return redirect('/import-custom-properties-file', 301)->with(compact('message'));
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
