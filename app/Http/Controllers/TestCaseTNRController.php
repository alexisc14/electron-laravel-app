<?php

namespace App\Http\Controllers;

use App\Models\TestCaseTNR;
use App\Models\TNR;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Process;

class TestCaseTNRController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $testCases = TestCaseTNR::orderBy('TestNumber', 'ASC')->get();
        $tnrs = TNR::where("success", '!=', '')->orderBy('created_at', 'desc')->get();
        return view("tnr.index", compact('testCases', 'tnrs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $testCase = TestCaseTNR::where('TestNumber', $request->testNumber)->first();

        $useCase = $testCase->UseCase;
        $cid = uniqid('test_'.str_replace(" ","_", strtolower($useCase)).'_').'_'.date('Ymd');
        $useCaseFolderName = $useCase."-".$cid;

        $arrAttributes = $testCase->attributesToArray();
        $arrAttributes['correlation_id'] = $cid;

        /** Removing unnecessary key */ 
        if (array_key_exists('id', $arrAttributes)) {
            unset($arrAttributes['id']);
        }
        if (array_key_exists('UseCase', $arrAttributes)) {
            unset($arrAttributes['UseCase']);
        }
        if (array_key_exists('created_at', $arrAttributes)) {
            unset($arrAttributes['created_at']);
        }
        if (array_key_exists('updated_at', $arrAttributes)) {
            unset($arrAttributes['updated_at']);
        }

        /** SOAPUI project properties params */ 
        $testRunnerParams = "-P";
        foreach ($arrAttributes as $key => $value) {
            if ($value != ""){
                $testRunnerParams .= $key . '="' . $value . '" -P';
            }
        }
        if (str_contains($useCase, 'void') || str_contains($useCase, 'VOID')){
            $testRunnerParams .= '"Cancellation=VOID" -P';
        }
        if (str_contains($useCase, 'refund') || str_contains($useCase, 'REFUND')){
            $testRunnerParams .= '"Cancellation=REFUND" -P';
        }

        $testRunnerParams = rtrim($testRunnerParams, ",");
        $testRunnerParams = rtrim($testRunnerParams, " -P");

        /** Command to execute */ 
        $cmdWithParams = '"'.env('READYAPI_TESTRUNNER_PATH').'" "-s'.env('READYAPI_TESTSUITE').'" "-c'.$useCase.'" '.$testRunnerParams.' "-f'.env('READYAPI_USECASE_LOGS')."\\$useCaseFolderName".'" "-RJUnit-Style HTML Report" -FXML "-E'.env('READYAPI_TESTSUITE_ENV').'" "'.env('READYAPI_PROJECT').'"';

        /** TNR DB insertion */ 
        $tnr = TNR::firstOrCreate([
            "useCase" => $useCase,
            "cid" => $cid,
            "created_at" => now()
        ]);

        // Setting temporarily 300 secs (5 min) to perform the TNR
        set_time_limit(300);

        /** Exec command */ 
        exec($cmdWithParams, $output, $retval);

        $resultTestCase = "";
        $statusTestCase = false;
        foreach ($output as $value) {
            if (strpos($value, 'TestCase ['.$useCase.'] finished with status [PASS]') !== false) {
                $statusTestCase = true;
                $resultTestCase = $value;
            }
        }

        $isRegression = "";
        if ($testCase->TestNumber == $request->testNumber && $statusTestCase === false){
            $isRegression = true;
        }

        if (!$statusTestCase){
            set_time_limit(0);
            $tnr = TNR::find($tnr->id);
            $tnr->status = "ERROR";
            $tnr->success = $retval;
            $tnr->isRegression = $isRegression;
            $tnr->updated_at = now();
            $tnr->save();
            $error = __('Controllers')['TestCaseTNR']['Functions']['storeErrorMessage1']."\"$tnr->useCase\" .".__('Controllers')['TestCaseTNR']['Functions']['storeErrorMessage2']."\"$tnr->cid\"";
            return response()->json(["status" => 400, "error" => $error]);
        }

        set_time_limit(0);
        $tnr = TNR::find($tnr->id);
        $tnr->status = "PASS";
        $tnr->success = $retval;
        $tnr->isRegression = $isRegression;
        $tnr->updated_at = now();
        $tnr->save();
        $message = $resultTestCase;
        return response()->json(["status" => 200, "message" => $message]);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $testCase = TNR::find($id);
        $formattedTestCase = str_replace(' ','-',$testCase->useCase);
        $publicLogsPathFailedFolder = "";
        $failedFileName = "";
        $logsFailedFolder = "";

        if ($testCase->status == "ERROR"){
            $logsFailedFolder = env('READYAPI_USECASE_LOGS')."\\$testCase->useCase-$testCase->cid";
            /** Copy in the project folder the index.html & associated stylesheet */
            $publicLogsPathFailedFolder = public_path("tnrexecs/$formattedTestCase");
            // Deleting old files to put new ones
            if (File::isDirectory($publicLogsPathFailedFolder))
                File::deleteDirectory($publicLogsPathFailedFolder);
            File::makeDirectory($publicLogsPathFailedFolder, 0755, true);
            $files = File::files($logsFailedFolder);
            foreach($files as $failedFile){
                $failedFileName = $failedFile->getFileName();
                File::copy($logsFailedFolder."\\$failedFileName", $publicLogsPathFailedFolder."/$failedFileName");
            }
        }
        return view("tnr.show", compact('testCase', 'publicLogsPathFailedFolder', 'formattedTestCase', 'failedFileName'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TestCaseTNR $testCaseTNR)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TestCaseTNR $testCaseTNR)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TestCaseTNR $testCaseTNR)
    {
        // Clean TNR
        $testCase = TestCaseTNR::find($testCaseTNR);
        $testCase->delete();
        $message = __('Controllers')['TestCaseTNR']['Functions']['destroy'];
        return view("tnr.index", compact('message'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyAll()
    {
        /** Delete the entire table + reset the auto-incrementing IDs to zero */
        TNR::truncate();
        $message = __('Controllers')['TestCaseTNR']['Functions']['destroyAll'];
        return redirect('/tnr', 301)->with(compact('message'));
    }
}
