<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\TestCase;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class ExportAsCSVController extends Controller
{
    public static function export()
    {
        $csvFileName = env("CUSTOM_PROPERTIES_FILE");

        $columnNamesRaw = TestCase::getColumnName();

        $columnNamesHeaders = [];
        $columnNamesHeaders[] = "TestNumber";
        $columnNamesHeaders[] = "AirShoppingEndpoint";
        $columnNamesHeaders[] = "OfferPriceEndpoint";
        $columnNamesHeaders[] = "OrderCreateEndpoint";
        $columnNamesHeaders[] = "OrderCancelEndpoint";
        $columnNamesHeaders[] = "OrderChangeEndpoint";
        $columnNamesHeaders[] = "OrderReshopEndpoint";
        $columnNamesHeaders[] = "OrderRetrieveEndpoint";
        $columnNamesHeaders[] = "ServiceListEndPoint";
        $columnNamesHeaders[] = "SeatAvailabilityEndPoint";
        $columnNamesHeaders[] = "OrderHistoryEndPoint";

        
        /** Removing unnecessary columns */ 
        if (in_array('id', $columnNamesRaw)) {
            unset($columnNamesRaw[array_search('id',$columnNamesRaw)]);
        }
        if (in_array('TestNumber', $columnNamesRaw)) {
            unset($columnNamesRaw[array_search('TestNumber',$columnNamesRaw)]);
        }
        if (in_array('created_at', $columnNamesRaw)) {
            unset($columnNamesRaw[array_search('created_at',$columnNamesRaw)]);
        }
        if (in_array('updated_at', $columnNamesRaw)) {
            unset($columnNamesRaw[array_search('updated_at',$columnNamesRaw)]);
        }
        
        /** Push TestNumber (airline, iata, etc..) column name as header */
        foreach ($columnNamesRaw as $columnName) {
            $columnNamesHeaders[] = $columnName;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($columnNamesHeaders, NULL, 'A1');
    
        $ndcParametersValues = [];
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "001448v02";
        $ndcParametersValues[] = "002083v01";
        $ndcParametersValues[] = "001451v02";
        $ndcParametersValues[] = "001847v02";
        $ndcParametersValues[] = "001739v02";
        $ndcParametersValues[] = "002115v01";
        $ndcParametersValues[] = "001724v02";
        $ndcParametersValues[] = "001911v02";
        $ndcParametersValues[] = "001450v02";
        $ndcParametersValues[] = "002647v01";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";
        $ndcParametersValues[] = "";

        $sheet->fromArray($ndcParametersValues, NULL, 'A2');

        $testCases = TestCase::orderBy("TestNumber", 'ASC')->get();

        $testCasesValues = [];
        $cellIndex = 3;
        foreach ($testCases as $testCase) {
            $testCasesValues[] = $testCase->TestNumber;
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = 'rct';
            $testCasesValues[] = $testCase->correlation_id == null ? "" : $testCase->correlation_id;
            $testCasesValues[] = $testCase->airline == null ? "" : $testCase->airline;
            $testCasesValues[] = $testCase->pseudo_city == null ? "" : $testCase->pseudo_city;
            $testCasesValues[] = $testCase->iata_agency == null ? "" : $testCase->iata_agency;
            $testCasesValues[] = $testCase->travel_agency_name == null ? "" : $testCase->travel_agency_name;
            $testCasesValues[] = $testCase->cabin_type_Name == null ? "" : $testCase->cabin_type_Name;
            $testCasesValues[] = $testCase->StrictSearchCabin == null ? "" : $testCase->StrictSearchCabin;
            $testCasesValues[] = $testCase->StrictSearchFF == null ? "" : $testCase->StrictSearchFF;
            $testCasesValues[] = $testCase->bundle_seats == null ? "" : $testCase->bundle_seats;
            $testCasesValues[] = $testCase->GST_Tax == null ? "" : $testCase->GST_Tax;
            $testCasesValues[] = $testCase->upsell == null ? "" : $testCase->upsell;
            $testCasesValues[] = $testCase->bundleSeatAtUpsell == null ? "" : $testCase->bundleSeatAtUpsell;
            $testCasesValues[] = $testCase->countrycode == null ? "" : $testCase->countrycode;
            $testCasesValues[] = $testCase->pax == null ? "" : $testCase->pax;
            $testCasesValues[] = $testCase->paymentMethod == null ? "" : $testCase->paymentMethod;
            $testCasesValues[] = $testCase->recognition == null ? "" : $testCase->recognition;
            $testCasesValues[] = $testCase->MultiplePricingProgram == null ? "" : $testCase->MultiplePricingProgram;
            $testCasesValues[] = $testCase->pricing_program == null ? "" : $testCase->pricing_program;
            $testCasesValues[] = $testCase->account_code == null ? "" : $testCase->account_code;
            $testCasesValues[] = $testCase->OIN == null ? "" : $testCase->OIN;
            $testCasesValues[] = $testCase->OnD == null ? "" : $testCase->OnD;
            $testCasesValues[] = $testCase->OnDRebooking == null ? "" : $testCase->OnDRebooking;
            $testCasesValues[] = $testCase->Date == null ? "" : $testCase->Date;
            $testCasesValues[] = $testCase->DateRebooking == null ? "" : $testCase->DateRebooking;
            $testCasesValues[] = $testCase->order_id == null ? "" : $testCase->order_id;
            $testCasesValues[] = $testCase->private_fare == null ? "" : $testCase->private_fare;
            $testCasesValues[] = $testCase->Bag == null ? "" : $testCase->Bag;
            $testCasesValues[] = $testCase->BagRebook == null ? "" : $testCase->BagRebook;
            $testCasesValues[] = $testCase->Seat == null ? "" : $testCase->Seat;
            $testCasesValues[] = $testCase->SeatRebook == null ? "" : $testCase->SeatRebook;
            $testCasesValues[] = $testCase->FareFamilyRebook == null ? "" : $testCase->FareFamilyRebook;
            $testCasesValues[] = $testCase->APIS == null ? "" : $testCase->APIS;
            $testCasesValues[] = $testCase->APIS_type == null ? "" : $testCase->APIS_type;
            $testCasesValues[] = $testCase->APIS_update_pax == null ? "" : $testCase->APIS_update_pax;
            $testCasesValues[] = $testCase->APIS_pax_titlename == null ? "" : $testCase->APIS_pax_titlename;
            $testCasesValues[] = $testCase->APIS_after_create == null ? "" : $testCase->APIS_after_create;
            $testCasesValues[] = $testCase->LoyaltyProgramUpdatePax == null ? "" : $testCase->LoyaltyProgramUpdatePax;
            $testCasesValues[] = $testCase->LoyaltyProgramAfterBooking == null ? "" : $testCase->LoyaltyProgramAfterBooking;
            $testCasesValues[] = $testCase->LoyaltyProgram == null ? "" : $testCase->LoyaltyProgram;
            $testCasesValues[] = $testCase->LoyaltyProgramStep == null ? "" : $testCase->LoyaltyProgramStep;
            $testCasesValues[] = $testCase->LoyaltyProgramTierName == null ? "" : $testCase->LoyaltyProgramTierName;
            $testCasesValues[] = $testCase->LP_programName == null ? "" : $testCase->LP_programName;
            $testCasesValues[] = $testCase->LP_account == null ? "" : $testCase->LP_account;
            $testCasesValues[] = $testCase->LP_civility == null ? "" : $testCase->LP_civility;
            $testCasesValues[] = $testCase->LP_surname == null ? "" : $testCase->LP_surname;
            $testCasesValues[] = $testCase->LP_givenname == null ? "" : $testCase->LP_givenname;
            $testCasesValues[] = $testCase->split_pax_to_update == null ? "" : $testCase->split_pax_to_update;
            $sheet->fromArray($testCasesValues, NULL, 'A'.$cellIndex++);
            unset($testCasesValues);
        }

        $writer = new Csv($spreadsheet);
        $writer->setDelimiter(';');
        $writer->setLineEnding("\r\n");
        $writer->setSheetIndex(0);

        $writer->save($csvFileName);

        /** Removing the '"' character in .csv file */ 
        $csvContent = file_get_contents($csvFileName);
        $csvContent = str_replace('"', '', $csvContent);
        file_put_contents($csvFileName, $csvContent);

        $message = __('Controllers')['ExportAsCSV']['Functions']['export'];
        $message = "Export to \"$csvFileName\" finished";
        return response()->json(["status" => 200, "message" => $message]);
    }
}
