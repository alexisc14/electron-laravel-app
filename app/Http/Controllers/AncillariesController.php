<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AncillariesName;

class AncillariesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $ancillaryTypes = explode(" ", __('AncillariesPage')['ancillariesFormTypes']);
        return view('ancillariesname.create', compact('ancillaryTypes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        AncillariesName::firstOrCreate([
            'name' => $request->ancillaries_name,
            'name_description' => $request->ancillaries_name_description,
            'type' => $request->ancillaries_type,
            'created_at' => now()
        ]);

        $messageTemplate = __('Controllers')['Ancillaries']['Functions']['store'];
        $message = str_replace(":ancillary", $request->ancillaries_name, $messageTemplate);
        return redirect('/', 301)->with(compact('message'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $ancillaries = AncillariesName::find($id);
        $ancillaryTypes = explode(" ", __('AncillariesPage')['ancillariesFormTypes']);
        return view('ancillariesname.show', compact('ancillaries', 'ancillaryTypes'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $ancillary = AncillariesName::find($id);
        $ancillary->name = $request->ancillaries_name;
        $ancillary->name_description = $request->ancillaries_name_description;
        $ancillary->type = $request->ancillaries_type;
        $ancillary->updated_at = now();
        $ancillary->save();
        $messageTemplate = __('Controllers')['Ancillaries']['Functions']['update'];
        $message = str_replace(":ancillary", $ancillary->name, $messageTemplate);
        return redirect('/', 301)->with(compact('message'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $ancillary = AncillariesName::find($id);
        $messageTemplate = __('Controllers')['Ancillaries']['Functions']['destroy'];
        $message = str_replace(":ancillary", $ancillary->name, $messageTemplate);
        $ancillary->delete();
        return redirect('/', 301)->with(compact('message'));
    }
}
