<?php

namespace App\Http\Controllers;

use App;

use App\Models\TestCase;
use App\Models\FareFamily;
use App\Models\AncillariesName;
use App\Http\Controllers\TestCaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    
    public function index(){
        $testCases = TestCase::orderBy('TestNumber', 'ASC')->get();
        $lastUpdatedTestCase = TestCase::orderBy('updated_at', 'DESC')->first();
        $fareFamilies = FareFamily::all();
        $ancillariesName = AncillariesName::all();
        return view('index', compact('testCases', 'fareFamilies', 'ancillariesName', 'lastUpdatedTestCase'));
    }

    public function changeLang(Request $request)
    {
        App::setLocale($request->lang);
        Session::put("localeLang", $request->lang);
        return redirect()->back(); // redirect in the same page where the translation is done
    }
}
