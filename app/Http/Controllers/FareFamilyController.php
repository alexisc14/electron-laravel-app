<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FareFamily;



class FareFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('farefamily.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        FareFamily::firstOrCreate([
            'cabin_type' => $request->cabin_type_name,
            'fare_family_name' => $request->fare_family_name,
            'created_at' => now()
        ]);

        $messageTemplate = __('Controllers')['FareFamily']['Functions']['store'];
        $message = str_replace([":cabinType", ":fareFamily"], [$request->cabin_type_name, $request->fare_family_name], $messageTemplate);
        return redirect('/fare-family/create', 301)->with(compact('message'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $fareFamily = FareFamily::find($id);
        return view('farefamily.show', compact('fareFamily'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $fareFamily = FareFamily::find($id);
        $fareFamily->cabin_type = $request->cabin_type_name;
        $fareFamily->fare_family_name = $request->fare_family_name;
        $fareFamily->updated_at = now();
        $fareFamily->save();
        $messageTemplate = __('Controllers')['FareFamily']['Functions']['update'];
        $message = str_replace([":cabinType", ":fareFamily"], [$fareFamily->cabin_type, $fareFamily->fare_family_name], $messageTemplate);
        return redirect('/', 301)->with(compact('message'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $fareFamily = FareFamily::find($id);
        $messageTemplate = __('Controllers')['FareFamily']['Functions']['destroy'];
        $message = str_replace([":cabinType", ":fareFamily"], [$fareFamily->cabin_type, $fareFamily->fare_family_name], $messageTemplate);
        $fareFamily->delete();
        return redirect('/', 301)->with(compact('message'));
    }
}
