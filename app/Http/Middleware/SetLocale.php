<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Session;
use Config;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Session::has('localeLang')){
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            $acceptLang = ['fr', 'en'];
            $lang = in_array($lang, $acceptLang) ? $lang : 'en';        
            App::setLocale(Session::get("localeLang"));

        } else {
            Session::put("localeLang", "fr");
            App::setLocale(Session::get("localeLang"));
        }

        return $next($request);
    }
}
