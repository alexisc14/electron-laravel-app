<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TestCase extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'testcase';
    protected $hidden = ['created_at','updated_at'];
    protected $primaryKey = 'id';

    public static function getColumnName()
    {        
        return \DB::getSchemaBuilder()->getColumnListing('testcase');
    }
}
