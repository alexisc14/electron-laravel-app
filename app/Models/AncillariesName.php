<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AncillariesName extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'ancillariesname';
    protected $primaryKey = 'id';

}
