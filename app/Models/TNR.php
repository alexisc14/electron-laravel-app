<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TNR extends Model
{
    use HasFactory;
    protected $connection = 'sqlite_tnr';
    protected $table = 'TNR';
    protected $primaryKey = 'id';
    protected $fillable = ['useCase', 'cid'];

}
