<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestCaseTNR extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $connection = 'sqlite_tnr';
    protected $table = 'testcase';
    //protected $fillable = 'TestNumber';
    protected $primaryKey = 'id';
}
