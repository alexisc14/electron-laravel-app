<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FareFamily extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'farefamily';
    protected $primaryKey = 'id';
}
